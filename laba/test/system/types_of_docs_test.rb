require "application_system_test_case"

class TypesOfDocsTest < ApplicationSystemTestCase
  setup do
    @types_of_doc = types_of_docs(:one)
  end

  test "visiting the index" do
    visit types_of_docs_url
    assert_selector "h1", text: "Types Of Docs"
  end

  test "creating a Types of doc" do
    visit types_of_docs_url
    click_on "New Types Of Doc"

    click_on "Create Types of doc"

    assert_text "Types of doc was successfully created"
    click_on "Back"
  end

  test "updating a Types of doc" do
    visit types_of_docs_url
    click_on "Edit", match: :first

    click_on "Update Types of doc"

    assert_text "Types of doc was successfully updated"
    click_on "Back"
  end

  test "destroying a Types of doc" do
    visit types_of_docs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Types of doc was successfully destroyed"
  end
end
