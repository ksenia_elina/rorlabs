require 'test_helper'

class TypesOfDocsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @types_of_doc = types_of_docs(:one)
  end

  test "should get index" do
    get types_of_docs_url
    assert_response :success
  end

  test "should get new" do
    get new_types_of_doc_url
    assert_response :success
  end

  test "should create types_of_doc" do
    assert_difference('TypesOfDoc.count') do
      post types_of_docs_url, params: { types_of_doc: {  } }
    end

    assert_redirected_to types_of_doc_url(TypesOfDoc.last)
  end

  test "should show types_of_doc" do
    get types_of_doc_url(@types_of_doc)
    assert_response :success
  end

  test "should get edit" do
    get edit_types_of_doc_url(@types_of_doc)
    assert_response :success
  end

  test "should update types_of_doc" do
    patch types_of_doc_url(@types_of_doc), params: { types_of_doc: {  } }
    assert_redirected_to types_of_doc_url(@types_of_doc)
  end

  test "should destroy types_of_doc" do
    assert_difference('TypesOfDoc.count', -1) do
      delete types_of_doc_url(@types_of_doc)
    end

    assert_redirected_to types_of_docs_url
  end
end
