class Person < ApplicationRecord
    belongs_to :address
    has_many :document
    accepts_nested_attributes_for :address
    accepts_nested_attributes_for :document
end
