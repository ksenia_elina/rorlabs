class Document < ApplicationRecord
    belongs_to :person, optional: true
    belongs_to :country, optional: true
    belongs_to :types_of_doc
end
