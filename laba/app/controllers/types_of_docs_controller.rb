class TypesOfDocsController < ApplicationController
  before_action :set_types_of_doc, only: [:show, :edit, :update, :destroy]

  # GET /types_of_docs
  # GET /types_of_docs.json
  def index
    @types_of_docs = TypesOfDoc.all
  end

  # GET /types_of_docs/1
  # GET /types_of_docs/1.json
  def show
  end

  # GET /types_of_docs/new
  def new
    @types_of_doc = TypesOfDoc.new
  end

  # GET /types_of_docs/1/edit
  def edit
  end

  # POST /types_of_docs
  # POST /types_of_docs.json
  def create
    @types_of_doc = TypesOfDoc.new(types_of_doc_params)

    respond_to do |format|
      if @types_of_doc.save
        format.html { redirect_to @types_of_doc, notice: 'Types of doc was successfully created.' }
        format.json { render :show, status: :created, location: @types_of_doc }
      else
        format.html { render :new }
        format.json { render json: @types_of_doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /types_of_docs/1
  # PATCH/PUT /types_of_docs/1.json
  def update
    respond_to do |format|
      if @types_of_doc.update(types_of_doc_params)
        format.html { redirect_to @types_of_doc, notice: 'Types of doc was successfully updated.' }
        format.json { render :show, status: :ok, location: @types_of_doc }
      else
        format.html { render :edit }
        format.json { render json: @types_of_doc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /types_of_docs/1
  # DELETE /types_of_docs/1.json
  def destroy
    @types_of_doc.destroy
    respond_to do |format|
      format.html { redirect_to types_of_docs_url, notice: 'Types of doc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_types_of_doc
      @types_of_doc = TypesOfDoc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def types_of_doc_params
      params.fetch(:types_of_doc, {})
    end
end
