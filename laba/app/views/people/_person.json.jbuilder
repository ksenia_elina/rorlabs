json.extract! person, :id, :first_name, :last_name, :second_name, :sex, :date_birthday, :address, :created_at, :updated_at
json.url person_url(person, format: :json)
