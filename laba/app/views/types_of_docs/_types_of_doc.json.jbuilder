json.extract! types_of_doc, :id, :created_at, :updated_at
json.url types_of_doc_url(types_of_doc, format: :json)
