json.extract! address, :id, :town, :index, :street, :flat, :house, :created_at, :updated_at
json.url address_url(address, format: :json)
