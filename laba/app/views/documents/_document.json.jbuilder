json.extract! document, :id, :number, :type, :serial, :source, :people, :date, :country, :created_at, :updated_at
json.url document_url(document, format: :json)
