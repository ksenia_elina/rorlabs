Rails.application.routes.draw do
  resources :types_of_docs
  resources :types
  resources :addresses
  resources :countries
  resources :documents
  resources :people
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
