class Changeref < ActiveRecord::Migration[5.2]
  def change
    change_table :people do |t|
     # t.remove :first_name, :first_name, :last_name, :second_name,:sex, :date_birthday,:address
      #t.string :first_name, null:false, limit: 32
      #t.string :last_name, null:false, limit: 32
      #t.string :second_name, null:false, limit: 32
      #t.boolean :sex, null:false
      #t.date :date_birthday, null:false
      t.remove :address
      t.references :address, index: true, foreign_key: true
      #t.index :address_id, unique:true
    end
  end
end
