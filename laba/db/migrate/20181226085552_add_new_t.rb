class AddNewT < ActiveRecord::Migration[5.2]
  def change
    change_table :documents do |t|
      t.references :types_of_doc, index: true, foreign_key: true
      remove_reference(:documents, :types, index:true)
    end
  end
end
