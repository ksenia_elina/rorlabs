class AddRefPeople < ActiveRecord::Migration[5.2]
  def change
    change_table :documents do |t|
      t.remove :people
      t.references :people, index: true, foreign_key: true
    end 
    end
end
