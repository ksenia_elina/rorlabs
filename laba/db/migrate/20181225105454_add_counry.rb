class AddCounry < ActiveRecord::Migration[5.2]
  def change
    change_table :addresses do |t|
      t.references :country, index: true, foreign_key: true
      #t.timestamps
    end
  end
end
