class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :first_name
      t.string :last_name
      t.string :second_name
      t.boolean :sex
      t.date :date_birthday
      t.integer :address

      t.timestamps
    end
  end
end
