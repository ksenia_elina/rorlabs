class ChangePeoples < ActiveRecord::Migration[5.2]
  def change
    change_table :people do |t|
     # t.remove :first_name, :first_name, :last_name, :second_name,:sex, :date_birthday,:address
      #t.string :first_name, null:false, limit: 32
      #t.string :last_name, null:false, limit: 32
      #t.string :second_name, null:false, limit: 32
      #t.boolean :sex, null:false
      #t.date :date_birthday, null:false
      #t.references :address, index: true, foreign_key: true
      #t.index :address_id, unique:true

      t.index [:first_name, :last_name,:second_name, :date_birthday], unique: true, :name => 'my_index_p'

      #t.timestamps

      change_column(:people, :first_name, :string, null:false, limit: 32)
      change_column(:people, :last_name,:string, null:false, limit: 32)
      change_column(:people, :second_name, :string,null:false, limit: 32)
      change_column(:people, :sex,:string, null:false)
      change_column(:people, :date_birthday, :date, null:false)
    end
  end
end
