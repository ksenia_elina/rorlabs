class ChangeDocuments < ActiveRecord::Migration[5.2]
  def change
    change_table :documents do |t|
      change_column( :documents, :number,:string,  limit: 16,null:false)
      t.references :types, index: true, foreign_key: true
      change_column(:documents, :serial, :string, limit: 16)
      change_column(:documents,:source,:string, limit: 128)
      #change_column(:documents,:string,:people)
      change_column(:documents,:date, :date)
      t.references :countries, index: true, foreign_key: true

      t.index [:serial, :number,:type], unique: true
      #t.timestamps
    end
  end
end
