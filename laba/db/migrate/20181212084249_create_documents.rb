class CreateDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
      t.string :number
      t.integer :type
      t.string :serial
      t.string :source
      t.string :people
      t.date :date
      t.integer :country

      t.timestamps
    end
  end
end
