class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :town
      t.string :index
      t.string :street
      t.string :flat
      t.string :house

      t.timestamps
    end
  end
end
