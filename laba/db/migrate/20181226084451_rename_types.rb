class RenameTypes < ActiveRecord::Migration[5.2]
  def change
    rename_table :types, :types_of_doc
  end
end
