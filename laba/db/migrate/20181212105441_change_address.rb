class ChangeAddress < ActiveRecord::Migration[5.2]
  def change
    change_table :addresses do |t|
      change_column(:addresses, :town, :string, null:false, limit: 64)
      change_column(:addresses, :index, :string, limit: 6)
      change_column(:addresses,:street,:string, limit: 64)
      change_column(:addresses, :flat, :string, limit: 16)
      change_column(:addresses,:house, :string, limit: 16)

      #t.timestamps
    end
  end
end
