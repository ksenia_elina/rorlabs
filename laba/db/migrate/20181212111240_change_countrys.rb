class ChangeCountrys < ActiveRecord::Migration[5.2]
  def change
    change_table :countries do |t|
      change_column(:countries,:code, :string, limit: 3, null:false)
      change_column(:countries , :name, :string, null:false)

      #t.timestamps
    end
  end
end
