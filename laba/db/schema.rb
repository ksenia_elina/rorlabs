# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_26_095534) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "adminpack"
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "town", limit: 64, null: false
    t.string "index", limit: 6
    t.string "street", limit: 64
    t.string "flat", limit: 16
    t.string "house", limit: 16
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "country_id"
    t.index ["country_id"], name: "index_addresses_on_country_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "code", limit: 3, null: false
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "number", limit: 16, null: false
    t.integer "type"
    t.string "serial", limit: 16
    t.string "source", limit: 128
    t.date "date"
    t.integer "country"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "countries_id"
    t.bigint "types_of_doc_id"
    t.bigint "people_id"
    t.index ["countries_id"], name: "index_documents_on_countries_id"
    t.index ["people_id"], name: "index_documents_on_people_id"
    t.index ["serial", "number", "type"], name: "index_documents_on_serial_and_number_and_type", unique: true
    t.index ["types_of_doc_id"], name: "index_documents_on_types_of_doc_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "first_name", limit: 32, null: false
    t.string "last_name", limit: 32, null: false
    t.string "second_name", limit: 32, null: false
    t.string "sex", null: false
    t.date "date_birthday", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "address_id"
    t.index ["address_id"], name: "index_people_on_address_id"
    t.index ["first_name", "last_name", "second_name", "date_birthday"], name: "my_index_p", unique: true
  end

  create_table "types_of_doc", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "types_of_docs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "addresses", "countries"
  add_foreign_key "documents", "countries", column: "countries_id"
  add_foreign_key "documents", "people", column: "people_id"
  add_foreign_key "documents", "types_of_docs"
  add_foreign_key "people", "addresses"
end
